arm64: eapolcfg.c
	cc -Wall -g -o eapolcfg-arm64 eapolcfg.c -Iheaders -Iheaders/EAP8021X -target arm64-apple-macos11  -framework EAP8021X -F/System/Library/PrivateFrameworks -framework CoreFoundation -framework SystemConfiguration -framework Security 

x86: eapolcfg.c
	cc -Wall -g -o eapolcfg-x86 eapolcfg.c -Iheaders -Iheaders/EAP8021X  -target x86_64-apple-macos10.12 -framework EAP8021X -F/System/Library/PrivateFrameworks -framework CoreFoundation -framework SystemConfiguration -framework Security 
	
all: arm64 x86
		lipo -create -output eapolcfg eapolcfg-arm64 eapolcfg-x86 	
		

clean:
	rm -f eapolcfg-arm64
	rm -f eapolcfg-x86
	rm -f eapolcfg
	rm -rf *.dSYM/
	
