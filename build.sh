#!/bin/sh -e
this_dir=$(dirname $0)
filename="eapolcfg"
version="1.0"

if [ -e "../build/bitbucket_creds.sh" ]; then
	
	source "../build/bitbucket_creds.sh"
fi

if [ -e "../build/app_store_creds.sh" ]; then
	source "../build/app_store_creds.sh"
fi

make clean

make all 


codesign --verbose --force --deep -o runtime --sign "041493A13C3B9DA0EBA148448F12AEECA6FE7739" "${filename}" 

if [ $? -ne 0 ] ; then 
	
	echo "no codesigning. stopping."
fi

if [ ! "${app_store_id}" ]; then
	
	echo "no app store credentials so not uploading for notarization"

fi
zip  "${filename}-${version}.zip" "${filename}"

uuid=$(uuidgen)
echo "Uploading ${filename} to apple to notarize"
date
notarize_uuid=$(xcrun altool --notarize-app --primary-bundle-id "${uuid}" --username "${app_store_id}" --password "${app_store_password}" --file "${filename}-${version}.zip" 2>&1 |grep RequestUUID | awk '{print $3'})
date	
echo notarize_uuid is $notarize_uuid 	
success=0
while true ; do
	echo "Checking progress..."
	progress=$(xcrun altool --notarization-info "${notarize_uuid}"  -u "${app_store_id}" -p "${app_store_password}" 2>&1 )
	echo "${progress}"
	
	if [[  "${progress}" =~ "Invalid" ]] ; then
		echo "Error with checking notarization. trying again"
		break
	fi
	
	if [[  "${progress}" =~ "success" ]]; then
		echo SUCCESS
		success=1
		break
	else 
		echo "Not completed yet. Sleeping for 30 seconds"
	fi
	sleep 30
done
	


if [ ! "${bitbucket_username}" ]; then
	echo no bitbucket credentials so not uploading
	
fi

echo "Uploading ${filename}-${version}.zip"
if [ -f "${filename}.zip" ]; then

	curl --progress-bar -X POST "https://${bitbucket_username}:${bitbucket_password}@api.bitbucket.org/2.0/repositories/twocanoes/eapolcfg/downloads" --form files=@"${filename}-${version}.zip" > /tmp/curl.log
	
	
fi





